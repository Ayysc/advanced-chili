#pragma once

#include "Graphics.h"
#include "Vec2.h"
#include <vector>

class CoordinateTransformer
{
public:
	CoordinateTransformer( Graphics& gfx )
		:
		gfx( gfx )
	{}
	void DrawClosedPolyLine( std::vector<Vec2> poly,Color c )
	{
		const Vec2 offset = { float( Graphics::ScreenWidth / 2 ),float( Graphics::ScreenHeight / 2 ) };
		for( auto& v : poly )
		{
			v.y *= -1.0f;
			v += offset;
		}
		gfx.DrawClosedPolyLine( poly,c );
	}
private:
	Graphics& gfx;
};